#!/usr/bin/env python
import SimpleHTTPServer
import SocketServer
import os
import socket
import time

leak=[]

class MyTCPServer(SocketServer.TCPServer):
    allow_reuse_address = True
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)

class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        if self.path == '/alive':
            self.wfile.write("<html><body><h1>alive</h1></body></html>")
        if self.path == '/leak':
            leak.append(os.urandom(10**6))
            self.wfile.write("<html><body><h1>leak</h1></body></html>")
        if self.path == '/stuck':
            time.sleep(5*60)

Handler = MyRequestHandler
server = MyTCPServer(('0.0.0.0', 8080), Handler)

server.serve_forever()
